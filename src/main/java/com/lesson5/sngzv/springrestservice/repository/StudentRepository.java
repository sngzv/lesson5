package com.lesson5.sngzv.springrestservice.repository;

import com.lesson5.sngzv.springrestservice.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Integer> {
}
