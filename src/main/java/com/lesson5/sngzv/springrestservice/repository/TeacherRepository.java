package com.lesson5.sngzv.springrestservice.repository;

import com.lesson5.sngzv.springrestservice.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher, Integer> {
}
