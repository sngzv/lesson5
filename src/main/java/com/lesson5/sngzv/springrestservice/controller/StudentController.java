package com.lesson5.sngzv.springrestservice.controller;

import com.lesson5.sngzv.springrestservice.model.Student;
import com.lesson5.sngzv.springrestservice.model.Teacher;
import com.lesson5.sngzv.springrestservice.repository.StudentRepository;
import com.lesson5.sngzv.springrestservice.service.StudentService;
import com.lesson5.sngzv.springrestservice.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class StudentController {

    private final StudentService service;
    private final TeacherService teacherService;

    @Autowired
    public StudentController(StudentService service, TeacherService teacherService) {
        this.service = service;
        this.teacherService = teacherService;
    }

    @GetMapping("/students")
    public String findAllStudent(Model model) {
        List<Student> list = service.findAll();
        model.addAttribute("students", list);
        return "student-list";
    }

    @GetMapping("/student-create")
    public String createStudentForm(Student student){
        return "student-create";
    }

    @PostMapping("/student-create")
    public String createStudent(Student student){
        service.save(student);
        return "redirect:/students";
    }

    @GetMapping("student-delete/{id}")
    public String deleteStudent(@PathVariable("id") Integer id){
        service.deleteById(id);
        return "redirect:/students";
    }

    @GetMapping("/student-update/{id}")
    public String updateStudentForm(@PathVariable("id") Integer id, Model model){
        Student student = service.findById(id);
        model.addAttribute("student", student);
        return "student-update";
    }

    @PostMapping("/student-update")
    public String updateStudent(Student student){
        service.save(student);
        return "redirect:/students";
    }

    @GetMapping("/teachers-student/{id}")
    public String studentHaveTeachers(@PathVariable("id") Integer id, Model model) {
        Student student = service.findById(id);
        model.addAttribute("studentHaveTeachers", student);
        return "student-teachers";
    }

    @GetMapping("/student-link-delete/{studentId}/{teacherId}")
    public String deleteLinkStudent(@PathVariable("studentId") Integer studentId, @PathVariable("teacherId") Integer teacherId) {
        Teacher teacher = teacherService.findById(teacherId);
//        service.findById(teacherId).getStudentSet().remove(student);
        //service.deleteById(teacherId);
        Student student = service.findById(studentId);
        student.removeTeacherSet(teacher);
        teacher.removeStudentSet(student);
        service.save(student);
        return "redirect:/teachers-student/" + studentId;
    }
}
