package com.lesson5.sngzv.springrestservice.controller;

import com.lesson5.sngzv.springrestservice.model.Student;
import com.lesson5.sngzv.springrestservice.model.Teacher;
import com.lesson5.sngzv.springrestservice.service.StudentService;
import com.lesson5.sngzv.springrestservice.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Set;

@Controller
public class TeacherController {

    private final TeacherService service;
    private final StudentService studentService;

    @Autowired
    public TeacherController(TeacherService service, StudentService studentService) {
        this.service = service;
        this.studentService = studentService;
    }

    @GetMapping("/teachers")
    public String findAllTeacher(Model model) {
        List<Teacher>  list = service.findAll();
        model.addAttribute("teachers", list);
        return "teacher-list";
    }

    @GetMapping("/teacher-create")
    public String createTeacherForm(Teacher teacher){
        return "teacher-create";
    }

    @PostMapping("/teacher-create")
    public String createTeacher(Teacher teacher){
        service.saveTeacher(teacher);
        return "redirect:/teachers";
    }

    @GetMapping("teacher-delete/{id}")
    public String deleteTeacher(@PathVariable("id") Integer id){
        service.deleteById(id);
        return "redirect:/teachers";
    }

    @GetMapping("/teacher-update/{id}")
    public String updateTeacherForm(@PathVariable("id") Integer id, Model model){
        Teacher teacher = service.findById(id);
        model.addAttribute("teacher", teacher);
        return "teacher-update";
    }

    @PostMapping("/teacher-update")
    public String updateTeacher(Teacher user){
        service.saveTeacher(user);
        return "redirect:/teacher";
    }

    @GetMapping("/students-teacher/{id}")
    public String teacherHaveStudents(@PathVariable("id") Integer id, Model model) {
        Teacher teacher = service.findById(id);
        model.addAttribute("teacherHaveStudents", teacher);
        return "teacher-students";
    }

//    @GetMapping("teacher-link-delete/{teacher}/{s}")
//    public String deleteLinkTeacher(@PathVariable("teacher") Teacher teacher, @PathVariable("s") Student student) {
//        service.findById(teacher.getId()).getStudentSet().remove(student);
//        return "redirect:/teachers";
//    }

    @GetMapping("/teacher-link-delete/{teacherId}/{studentId}")
    public String deleteLinkTeacher(@PathVariable("teacherId") Integer teacherId, @PathVariable("studentId") Integer studentId) {
        Student student = studentService.findById(studentId);
//        service.findById(teacherId).getStudentSet().remove(student);
         //service.deleteById(teacherId);
        Teacher teacher = service.findById(teacherId);
        teacher.removeStudentSet(student);
        //student.removeTeacherSet(teacher);
        service.saveTeacher(teacher);
        return "redirect:/students-teacher/" + teacherId;
    }

}
