package com.lesson5.sngzv.springrestservice.service;

import com.lesson5.sngzv.springrestservice.model.Teacher;
import com.lesson5.sngzv.springrestservice.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {

    private final TeacherRepository repository;

    @Autowired
    public TeacherService(TeacherRepository repository) {
        this.repository = repository;
    }

    public Teacher findById(Integer id){
        return repository.getOne(id);
    }

    public List<Teacher> findAll(){
        return repository.findAll();
    }

    public Teacher saveTeacher(Teacher user){
        return repository.save(user);
    }

    public void deleteById(Integer id){
        repository.deleteById(id);
    }
}
