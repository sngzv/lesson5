package com.lesson5.sngzv.springrestservice.service;

import com.lesson5.sngzv.springrestservice.model.Student;
import com.lesson5.sngzv.springrestservice.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    private final StudentRepository repository;

    @Autowired
    public StudentService(StudentRepository repository) {
        this.repository = repository;
    }

    public Student findById(Integer id){
        return repository.getOne(id);
    }

    public List<Student> findAll(){
        return repository.findAll();
    }

    public Student save(Student user){
        return repository.save(user);
    }

    public void deleteById(Integer id){
        repository.deleteById(id);
    }
}
