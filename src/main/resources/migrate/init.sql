CREATE TABLE teacher (
    id SERIAL,
    first_name VARCHAR(20),
    last_name VARCHAR(25),
    subject VARCHAR(25)
);

CREATE TABLE students (
    id SERIAL,
    first_name VARCHAR(20),
    last_name VARCHAR(25),
    course INTEGER
);

CREATE TABLE teachers_to_students (
    teacher_id INTEGER NOT NULL,
    student_id INTEGER NOT NULL,
    PRIMARY KEY (teacher_id, student_id),
    FOREIGN KEY (teacher_id) REFERENCES teachers (id) ON DELETE CASCADE,
    FOREIGN KEY (student_id) REFERENCES students (id) ON DELETE CASCADE
);